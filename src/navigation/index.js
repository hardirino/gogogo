import * as React from 'react';
import { View, Text, StyleSheet} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import login from '../screens/login'
import imdb from '../screens/imdb'
import list from '../screens/list';

const Stack = createNativeStackNavigator();

function index(){
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={login} options={{headerShown: false}}/>
        <Stack.Screen name="imdb" component={imdb} options={{headerShown: false}}/>
        <Stack.Screen name="list" component={list} options={{headerShown: false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default index;