import React, { Component } from 'react'
import {
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Alert
}
    from 'react-native'

export default class list extends Component {
    render() {
        return (
            <View style={{ flex: 1 , backgroundColor: 'black'}}>
                <View style={model.container}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('imdb') }}>
                        <Text style={{
                            fontSize: 40,
                            fontWeight: 'bold',
                            color: 'pink',
                        }}
                        > Assignment 1
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={model.container}>
                    <TouchableOpacity>
                    <Text style={{
                            fontSize: 40,
                            fontWeight: 'bold',
                            color: 'pink'
                        }}
                        > Assignment 2
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={model.container}>
                    <TouchableOpacity>
                    <Text style={{
                            fontSize: 40,
                            fontWeight: 'bold',
                            color: 'pink'
                        }}
                        > Assignment 3
                        </Text>
                    </TouchableOpacity>
                </View>
            </View >
        )
    }
}

const model = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        borderWidth: 10,
        borderRadius: 30,
        borderColor: 'pink',
        backgroundColor: 'black',
        alignSelf: 'center',
        justifyContent: 'center',

    },
}
)