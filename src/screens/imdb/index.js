import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  Alert
} from 'react-native';

export default class imdb extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      isLoading: true,
    };
  }

  async getMovies() {
    try {
      const response = await fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4&');
      const json = await response.json();
      this.setState({ data: json.Search, isLoading: false });
    } catch (error) {
      console.log(error);
    }
  }

  componentDidMount() {
    this.getMovies();
  }

  render() {
    const { data, isLoading } = this.state;
    return (
      <View style={{ flex: 1, paddingTop: 10, backgroundColor: 'black' }}>
        <View style={styles.yourhead}>
          <Text style={styles.myhead}>Your Movies List
          </Text>
          <TouchableOpacity onPress={() => Alert.alert('Anda Tertypu')} 
          style={styles.thishead}>
            <Text style={{fontSize: 20, fontWeight: 'bold', alignSelf: 'center', color: 'black'}}>
              Login
              </Text>
          </TouchableOpacity>
        </View>
        {isLoading ? <ActivityIndicator /> : (
          <FlatList
            data={data}
            keyExtractor={({ imdbID }) => (imdbID)}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() =>
                  Alert.alert('This film is not released yet.')}>
                <View style={styles.biawak}>

                  <Image
                    style={{
                      width: 200,
                      height: 300,
                      marginLeft: 10,
                      marginTop: 10,
                      borderRadius: 30,
                    }}
                    source={{ uri: item.Poster }} />
                  <View style={styles.srepett}>
                    <Text
                      style={{
                        color: 'black',
                        fontSize: 18,
                        fontWeight: 'bold',
                        marginBottom: 5,
                        alignSelf: 'flex-start',
                        paddingLeft: 10,
                        paddingTop: 100,
                      }}>
                      {item.Title}
                    </Text>
                    <Text
                      style={{
                        color: 'black',
                        fontSize: 18,
                        fontWeight: 'bold',
                        marginBottom: 5,
                        alignSelf: 'flex-start',
                        paddingLeft: 5,
                      }}> Tahun :
                      {item.Year}
                    </Text>
                    <Text
                      style={{
                        color: 'black',
                        fontSize: 18,
                        fontWeight: 'bold',
                        marginBottom: 5,
                        alignSelf: 'flex-start',
                        paddingLeft: 5,
                      }}> Barcode :
                      {item.Type}
                    </Text>
                    <Text
                      style={{
                        color: 'black',
                        fontSize: 18,
                        fontWeight: 'bold',
                        marginBottom: 5,
                        alignSelf: 'flex-start',
                        paddingLeft: 5,
                      }}> Tipe :
                      {item.imdbID}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        )}
      </View>
    );
  }
};

const styles = StyleSheet.create({
  srepet: {
    flex: 1,
    flexDirection: 'column'
  },
  yourhead: {
    marginBottom: 10,
    alignSelf: 'center',
    flexDirection: 'row'
  },
  myhead: {
    color: 'black',
    backgroundColor: 'pink',
    fontWeight: 'bold',
    fontSize: 30,
    borderColor: 'blue',
    borderRadius: 15,
    height: 60,
    width: 300,
    justifyContent: 'center',
    textAlign: 'center',
    paddingTop: 9,
    marginLeft: 10,
  },
  thishead: {
    color: 'black',
    backgroundColor: 'pink',
    fontWeight: 'bold',
    fontSize: 20,
    borderRadius: 10,
    height: 60,
    width: 60,
    textAlign: 'center',
    paddingTop: 15,
    marginLeft: 25,
  },
  biawak: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'pink',
    borderRadius: 30,
    marginTop: 5,
    marginBottom: 5,
    paddingBottom: 10,
  },
  kocok: {
    flexDirection: 'column',
    paddingTop: 120,
    alignSelf: 'flex-start',
  }
})